# Rchapp

Es una app móvil el cual se encarga de mensajería entre usuarios como otras aplicaciones reconocidas cono WhatsApp o Telegram pero creada con fines de aprendizaje y como proyecto escolar, al igual que es usada de forma personal. 

# Authors

* Salvador Quintero - (Developer, Designer) [[Gitlab](https://gitlab.com/Shavatl)]

## Others
```
Si tienes dudas o quieres contribuir con el proyecto o mejorarlo te puedes poner en contacto conmigo desde mi correo 
crispinquintero@gmail.co o por este medio, gracias.

```
