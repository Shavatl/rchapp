package com.example.crisp.rchapp.Mensajeria;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.rchapp.BaseEstatica;
import com.example.crisp.rchapp.MRchapp;
import com.example.crisp.rchapp.R;
import com.example.crisp.rchapp.VolleyRP;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by crisp on 20/12/2017.
 */

public class Mensaje extends AppCompatActivity
{
    public static final String MENSAJE = "MENSAJE";
    private BroadcastReceiver BR;
    private RecyclerView rv;
    private CircleImageView btEnviarMensaje;
    private Button btCerrarSesion;
    private EditText etEscribirMensaje;
    private List<MensajeDeTexto> mensajeDeTextos;
    private MensajesAdapter adapter;

    private String enviar_mensaje = "";
    private String emisor = "";
    private String RECEPTOR;

    private static final String IP_mensaje = "https://crispinquintero08.000webhostapp.com/ArchivoPHP/Enviar_Mensajes.php";

    private VolleyRP volley;
    private RequestQueue mRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mensaje);

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        Intent i = getIntent();
        Bundle bundle = i.getExtras();

        if(bundle!=null)
        {
            RECEPTOR = bundle.getString("key_receptor");

        }

        emisor = BaseEstatica.obtenerCadena(this,BaseEstatica.shared_usuario);

        rv = (RecyclerView) findViewById(R.id.rvMensajes);
        mensajeDeTextos = new ArrayList<>();
        adapter = new MensajesAdapter(mensajeDeTextos, this);
        rv.setAdapter(adapter);

        btEnviarMensaje = (CircleImageView) findViewById(R.id.btEnviarMensaje);
        btCerrarSesion = (Button) findViewById(R.id.btCerrarSesion);

        etEscribirMensaje = (EditText) findViewById(R.id.etEscribirMensaje);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setStackFromEnd(true);
        rv.setLayoutManager(lm);

        btCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                finish();

            }
        });

        btEnviarMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mensaje = etEscribirMensaje.getText().toString().trim();
                String TOKEN = FirebaseInstanceId.getInstance().getToken();

                if(!mensaje.isEmpty())
                {
                    CreateMensaje(mensaje,"10:20",1);

                    enviar_mensaje = mensaje;
                    MandarMensaje();
                    etEscribirMensaje.setText("");
                }
            }
        });

        BR = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String mensaje = intent.getStringExtra("key_mensaje");
                String hora = intent.getStringExtra("key_hora");
                String horaParametros[]= hora.split(",");
                String emisor = intent.getStringExtra("key_emisorPHP");

                if(emisor.equals(RECEPTOR))
                {
                    CreateMensaje(mensaje, horaParametros[0],2);
                }

            }
        };

        setScrollbarChat();

    }

    private void MandarMensaje()
    {
        HashMap<String,String> hashMapToken = new HashMap<>();
        hashMapToken.put("emisor",emisor);
        hashMapToken.put("receptor",RECEPTOR);
        hashMapToken.put("mensaje",enviar_mensaje);

        JsonObjectRequest solicitud = new JsonObjectRequest(Request.Method.POST,IP_mensaje,new JSONObject(hashMapToken),
                new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject datos) {
                try
                {
                    Toast.makeText(Mensaje.this,datos.getString("resultado"),Toast.LENGTH_SHORT).show();
                }
                catch (JSONException e)
                {

                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(Mensaje.this,"Ocurrio un error",Toast.LENGTH_SHORT).show();
            }
        });
        VolleyRP.addToQueue(solicitud,mRequest,this,volley);

    }


    public void CreateMensaje(String mensaje,String hora, int tipoDeMensaje)
    {
        MensajeDeTexto mensajeDeTextoAuxiliar = new MensajeDeTexto();
        mensajeDeTextoAuxiliar.setId("0");
        mensajeDeTextoAuxiliar.setTipoMensaje(tipoDeMensaje);
        mensajeDeTextoAuxiliar.setMensaje(mensaje);
        mensajeDeTextoAuxiliar.setHoraDelMensaje(hora);
        mensajeDeTextos.add(mensajeDeTextoAuxiliar);
        adapter.notifyDataSetChanged();
        setScrollbarChat();


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(BR);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(BR, new IntentFilter(MENSAJE));

    }

    public void setScrollbarChat()
    {
        rv.scrollToPosition(adapter.getItemCount()-1);

    }
}
