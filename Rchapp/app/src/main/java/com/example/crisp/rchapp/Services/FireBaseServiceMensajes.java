package com.example.crisp.rchapp.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;

import com.example.crisp.rchapp.BaseEstatica;
import com.example.crisp.rchapp.Mensajeria.Mensaje;
import com.example.crisp.rchapp.R;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

/**
 * Created by crisp on 24/12/2017.
 */

public class FireBaseServiceMensajes extends FirebaseMessagingService
{
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String mensaje= remoteMessage.getData().get("mensaje");
        String hora= remoteMessage.getData().get("hora");
        String cabezera = remoteMessage.getData().get("cabezera");
        String cuerpo = remoteMessage.getData().get("cuerpo");
        String receptor = remoteMessage.getData().get("receptor");
        String emisorPHP = remoteMessage.getData().get("emisor");
        String emisor = BaseEstatica.obtenerCadena(this,BaseEstatica.shared_usuario);

        if (emisor.equals(receptor))
        {
            Mensajes(mensaje,hora,receptor,emisorPHP);
            MostrarNotificacion(cabezera,cuerpo);
        }
    }

    private void Mensajes(String mensaje, String hora,String receptor,String emisorPHP)
    {
        Intent i = new Intent(Mensaje.MENSAJE);
        i.putExtra("key_mensaje",mensaje);
        i.putExtra("key_hora",hora);
        i.putExtra("key_emisorPHP",emisorPHP);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
    }

    private void MostrarNotificacion(String cabezera,String cuerpo)
    {
        Intent i = new Intent(this,Mensaje.class);
        Uri sonidoDeNotificacion = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        PendingIntent pI = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_ONE_SHOT);
        Builder builder = new Builder(this);
        builder.setAutoCancel(true);
        builder.setContentTitle(cabezera);
        builder.setContentText(cuerpo);
        builder.setSound(sonidoDeNotificacion);
        builder.setSmallIcon(R.drawable.ic_action_key);
        builder.setContentIntent(pI);

        Random r = new Random();

        NotificationManager NM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NM.notify(r.nextInt(),builder.build());



    }
}
