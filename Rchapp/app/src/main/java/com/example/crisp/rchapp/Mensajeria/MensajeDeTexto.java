package com.example.crisp.rchapp.Mensajeria;

/**
 * Created by crisp on 21/12/2017.
 */

public class MensajeDeTexto
{
    private String id;
    private String mensaje;
    private String HoraDelMensaje;
    private int tipoMensaje;

    public MensajeDeTexto()
    {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHoraDelMensaje() {
        return HoraDelMensaje;
    }

    public void setHoraDelMensaje(String horaDelMensaje) {
        HoraDelMensaje = horaDelMensaje;
    }

    public int getTipoMensaje() {
        return tipoMensaje;
    }

    public void setTipoMensaje(int tipoMensaje) {
        this.tipoMensaje = tipoMensaje;
    }
}
