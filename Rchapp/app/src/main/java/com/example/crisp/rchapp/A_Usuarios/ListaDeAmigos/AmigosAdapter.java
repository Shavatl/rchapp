package com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.crisp.rchapp.Mensajeria.Mensaje;
import com.example.crisp.rchapp.R;

import java.util.List;

/**
 * Created by crisp on 29/12/2017.
 */

public class AmigosAdapter extends RecyclerView.Adapter<AmigosAdapter.HolderAmigos>
{
    private List<Atributos> atributosList;
    Context context;
    public AmigosAdapter(List<Atributos> atributosList, Context context)
    {
        this.atributosList = atributosList;
        this.context = context;

    }

    @Override
    public HolderAmigos onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cam_amigos,parent,false);
        return new AmigosAdapter.HolderAmigos(v);
    }

    @Override
    public void onBindViewHolder(HolderAmigos holder, final int position) {
        holder.imagen.setImageResource(R.drawable.ic_account_circle);
        holder.nombre.setText(atributosList.get(position).getNombre());
        holder.mensaje.setText(atributosList.get(position).getMensaje());
        holder.hora.setText(atributosList.get(position).getHora());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, Mensaje.class);
                i.putExtra("key_receptor",atributosList.get(position).getId());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    static class HolderAmigos extends RecyclerView.ViewHolder
    {
        CardView cardView;
        ImageView imagen;
        TextView nombre;
        TextView mensaje;
        TextView hora;

        public HolderAmigos(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardV);
            imagen = (ImageView) itemView.findViewById(R.id.fotoDePerfil);
            nombre = (TextView) itemView.findViewById(R.id.NombreAmigos);
            mensaje = (TextView) itemView.findViewById(R.id.MensajeAmigos);
            hora = (TextView) itemView.findViewById(R.id.HoraAmigos);
        }
    }

}
