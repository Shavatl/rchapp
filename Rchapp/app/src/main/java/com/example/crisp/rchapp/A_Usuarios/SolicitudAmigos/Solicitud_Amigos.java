package com.example.crisp.rchapp.A_Usuarios.SolicitudAmigos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos.AmigosAdapter;
import com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos.Atributos;
import com.example.crisp.rchapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crisp on 02/01/2018.
 */

public class Solicitud_Amigos extends Fragment
{
    private RecyclerView rv;
    private List<SolicitudAtributos> listSolicitudes;
    private SolicitudAdapter adapter;

    private LinearLayout sinSolicitud;

    public Solicitud_Amigos() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        listSolicitudes = new ArrayList<>();

        View v = inflater.inflate(R.layout.solicitud_amigos,container,false);
        sinSolicitud = (LinearLayout) v.findViewById(R.id.SolicitudVacio);
        rv = (RecyclerView) v.findViewById(R.id.rvSolicitudAmigos);

        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);

        adapter = new SolicitudAdapter(listSolicitudes,getContext());
        rv.setAdapter(adapter);

        actualizar();
        return v;
    }

    public void verificarSolicitud()
    {
        if(listSolicitudes.isEmpty())
        {
            sinSolicitud.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }
        else
        {
            sinSolicitud.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    public void AgregarSolicitudes(int fotoPerfil,String nombre,String hora)
    {
        SolicitudAtributos solicitud = new SolicitudAtributos();
        solicitud.setFotoPerfil(fotoPerfil);
        solicitud.setNombre(nombre);
        solicitud.setHora(hora);
        listSolicitudes.add(solicitud);
        actualizar();

    }

    public void actualizar()
    {
        adapter.notifyDataSetChanged();
        verificarSolicitud();

    }
}
