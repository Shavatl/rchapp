package com.example.crisp.rchapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.rchapp.A_Usuarios.Usuarios;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MRchapp extends AppCompatActivity
{
    private EditText Etuser;
    private EditText Etpassword;
    private Button Btkey;
    private Button registrar;

    private RadioButton rbSesion;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private String User="";
    private String Password="";

    private static String IP= "https://crispinquintero08.000webhostapp.com/ArchivoPHP/Login_GETID.php?id=";
    private static String IP_TOKEN = "https://crispinquintero08.000webhostapp.com/ArchivoPHP/Token_INSERTandUPDATE.php";

    private boolean activado;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrchapp);

        if(BaseEstatica.obtenerBoleano(this,BaseEstatica.shared_estado))
        {
            Intent i = new Intent(MRchapp.this, Usuarios.class);
            startActivity(i);
            finish();
        }

        volley = VolleyRP.getInstance(this);
        mRequest = volley.getRequestQueue();

        Etuser = (EditText) findViewById(R.id.Etuser);
        Etpassword = (EditText) findViewById(R.id.Etpassword);

        registrar = (Button) findViewById(R.id.registrar);
        Btkey = (Button) findViewById(R.id.Btkey);

        rbSesion = (RadioButton) findViewById(R.id.rbSesion);

        activado = rbSesion.isChecked();

        rbSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(activado)
                {
                    rbSesion.setChecked(false);
                }
                activado = rbSesion.isChecked();

            }
        });

        Btkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                verificacion(Etuser.getText().toString(),Etpassword.getText().toString());


            }
        });

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent i = new Intent(MRchapp.this, Registro.class);
                startActivity(i);


            }
        });
    }

    public void verificacion(String user, String password)
    {
        User=user;
        Password=password;
        requestJSON(IP+user);
    }

    public void requestJSON(String URL)
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject dato)
            {
                checkLoging(dato);

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(MRchapp.this,"Ocurrio un error.",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,this,volley);
    }

    public void checkLoging(JSONObject dato)
    {
        try
        {
            String estado = dato.getString("resultado");
            if(estado.equals("CC"))
            {
                JSONObject Jsondatos = new JSONObject(dato.getString("datos"));
                String usuario = Jsondatos.getString("id");
                String contraseña = Jsondatos.getString("Password");

                if(usuario.equals(User) && contraseña.equals(Password))
                {
                    String token = FirebaseInstanceId.getInstance().getToken();
                    if(token!=null)
                    {

                        MandarToken(token);
                    }
                    else
                    {
                        Toast.makeText(this,"Ocurrio un error, intentalo nuevamente",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(this,"Contraseña Incorrecta",Toast.LENGTH_SHORT).show();

                }

            }
            else
            {
                Toast.makeText(this,"No existe el usuario",Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e)
        {

        }

    }

    private void MandarToken(String token)
    {
        HashMap<String,String> HM = new HashMap<>();

        HM.put("id",User);
        HM.put("token",token);

        JsonObjectRequest requets = new JsonObjectRequest(Request.Method.POST,IP_TOKEN,new JSONObject(HM), new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject dato)
            {
                BaseEstatica.guardarBoleano(MRchapp.this,rbSesion.isChecked(),BaseEstatica.shared_estado);
                BaseEstatica.gardarCadena(MRchapp.this,User,BaseEstatica.shared_usuario);

                Intent i = new Intent(MRchapp.this, Usuarios.class);
                startActivity(i);
                finish();

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(MRchapp.this,"Intenta de ingresar de nuevo",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,this,volley);



    }



}
