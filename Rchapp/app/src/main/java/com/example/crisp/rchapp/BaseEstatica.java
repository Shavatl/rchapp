package com.example.crisp.rchapp;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by crisp on 28/12/2017.
 */

public class BaseEstatica
{
    public static final String shared = "rchapp";
    public static final String shared_estado = "rchapp.estado";
    public static final String shared_usuario = "rchapp.usuario";

    public static void guardarBoleano(Context c,boolean b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putBoolean(k,b).apply();
    }

    public static void gardarCadena(Context c,String b,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        preferences.edit().putString(k,b).apply();
    }

    public static boolean obtenerBoleano(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getBoolean(k,false);
    }

    public static String obtenerCadena(Context c,String k)
    {
        SharedPreferences preferences = c.getSharedPreferences(shared,c.MODE_PRIVATE);
        return preferences.getString(k,"");
    }

}
