package com.example.crisp.rchapp.A_Usuarios;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.crisp.rchapp.BaseEstatica;
import com.example.crisp.rchapp.MRchapp;
import com.example.crisp.rchapp.R;

/**
 * Created by crisp on 30/12/2017.
 */

public class Usuarios extends AppCompatActivity
{
    private TabLayout tlusuarios;
    private ViewPager vpusuarios;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuarios);
        setTitle("Rchapp");

        tlusuarios = (TabLayout) findViewById(R.id.tlUsuarios);
        vpusuarios = (ViewPager) findViewById(R.id.vpUsuarios);

        tlusuarios.setupWithViewPager(vpusuarios);

        vpusuarios.setAdapter(new Adaptador(getSupportFragmentManager()));

        vpusuarios.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {


            }

            @Override
            public void onPageSelected(int position)
            {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_amigos,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.CerrarSesion)
        {
            BaseEstatica.guardarBoleano(Usuarios.this,false,BaseEstatica.shared_estado);
            Intent i = new Intent(Usuarios.this, MRchapp.class);
            startActivity(i);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
