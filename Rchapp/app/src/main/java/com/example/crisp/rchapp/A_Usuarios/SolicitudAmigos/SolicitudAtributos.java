package com.example.crisp.rchapp.A_Usuarios.SolicitudAmigos;

/**
 * Created by crisp on 03/01/2018.
 */

public class SolicitudAtributos
{
    private int fotoPerfil;
    private String Nombre;
    private String Hora;

    public String getHora() {
        return Hora;
    }

    public void setHora(String hora) {
        Hora = hora;
    }

    public int getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(int fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
