package com.example.crisp.rchapp.A_Usuarios;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos.Amigos;
import com.example.crisp.rchapp.A_Usuarios.SolicitudAmigos.Solicitud_Amigos;

/**
 * Created by crisp on 31/12/2017.
 */

public class Adaptador extends FragmentPagerAdapter{

    public Adaptador(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position)
    {
        if(position==0)
        {
            return new Amigos();

        }

        if(position==1)
        {
            return new Solicitud_Amigos();

        }
        return null;
    }

    @Override
    public int getCount()
    {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0)
        {
            return "Chat";

        }
        if(position==1)
        {
            return "Solicitud";

        }
        return null;
    }
}
