package com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.crisp.rchapp.BaseEstatica;
import com.example.crisp.rchapp.R;
import com.example.crisp.rchapp.VolleyRP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crisp on 29/12/2017.
 */

public class Amigos extends Fragment
{

    private RecyclerView rv;
    private List<Atributos> atributosList;
    private AmigosAdapter adapter;

    private LinearLayout amigosVacio;

    private VolleyRP volley;
    private RequestQueue mRequest;

    private static final String URL_Usuarios = "https://crispinquintero08.000webhostapp.com/ArchivoPHP/Amigos_GETALL.php";

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.amigos,container,false);

        volley = VolleyRP.getInstance(getContext());
        mRequest = volley.getRequestQueue();

        atributosList = new ArrayList<>();
        amigosVacio = (LinearLayout) v.findViewById(R.id.AmigosVacio);
        rv = (RecyclerView) v.findViewById(R.id.amigos);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);

        adapter = new AmigosAdapter(atributosList,getContext());
        rv.setAdapter(adapter);

        requestJSON();

        actualizar();

        return v;
    }

    public void verificarSolicitud()
    {
        if(atributosList.isEmpty())
        {
            amigosVacio.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }
        else
        {
            amigosVacio.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    public void agregarAmigo(int foto,String nombre,String mensaje,String hora,String id)
    {
        Atributos atributos = new Atributos();
        atributos.setFotoDePerfil(foto);
        atributos.setNombre(nombre);
        atributos.setMensaje(mensaje);
        atributos.setHora(hora);
        atributos.setId(id);
        atributosList.add(atributos);
        actualizar();
    }

    public void actualizar()
    {
        adapter.notifyDataSetChanged();
        verificarSolicitud();

    }

    public void requestJSON()
    {
        JsonObjectRequest requets = new JsonObjectRequest(URL_Usuarios, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject dato)
            {
                try
                {
                    String TodosLosDatos = dato.getString("resultado");
                    String TodosLosUsuariosQueTienenToken = dato.getString("usuariosConTokens");
                    JSONArray jsonArray = new JSONArray(TodosLosDatos);
                    String NuestroUsuario = BaseEstatica.obtenerCadena(getContext(),BaseEstatica.shared_usuario);
                    JSONArray jsUserTokens = new JSONArray(TodosLosUsuariosQueTienenToken);
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject js = jsonArray.getJSONObject(i);
                        if(!NuestroUsuario.equals(js.getString("id")))
                        {
                            for(int k=0;k<jsUserTokens.length();k++)
                            {
                                JSONObject UsuarioConTokens = jsUserTokens.getJSONObject(k);
                                if(js.getString("id").equals(UsuarioConTokens.getString("id")))
                                {
                                    agregarAmigo(R.drawable.ic_account_circle,
                                            js.getString("nombre")+" "+ js.getString("apellidos"),
                                            "mensaje "+i,"00:00",js.getString("id"));
                                }
                            }

                        }
                    }
                }
                catch (JSONException e)
                {
                    Toast.makeText(getContext(),"Ocurrio un error.",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getContext(),"Ocurrio un error.",Toast.LENGTH_SHORT).show();

            }
        });
        VolleyRP.addToQueue(requets,mRequest,getContext(),volley);
    }
}

