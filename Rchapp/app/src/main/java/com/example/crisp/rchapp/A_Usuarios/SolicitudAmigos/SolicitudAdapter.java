package com.example.crisp.rchapp.A_Usuarios.SolicitudAmigos;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos.AmigosAdapter;
import com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos.Atributos;
import com.example.crisp.rchapp.R;

import java.util.List;

/**
 * Created by crisp on 02/01/2018.
 */

public class SolicitudAdapter extends RecyclerView.Adapter<SolicitudAdapter.solicitudHolder>
{
    private List<SolicitudAtributos> listSolicitudes;
    Context context;
    public SolicitudAdapter(List<SolicitudAtributos> listSolicitudes, Context context)
    {
        this.listSolicitudes = listSolicitudes;
        this.context = context;

    }

    @Override
    public solicitudHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cam_solicitud_amigos,parent,false);
        return new SolicitudAdapter.solicitudHolder(v);
    }

    @Override
    public void onBindViewHolder(solicitudHolder holder, int position)
    {
        holder.fotoPerfil.setImageResource(listSolicitudes.get(position).getFotoPerfil());
        holder.nombre.setText(listSolicitudes.get(position).getNombre());
        holder.hora.setText(listSolicitudes.get(position).getHora());
    }

    @Override
    public int getItemCount()
    {
        return listSolicitudes.size();
    }

    static class solicitudHolder extends RecyclerView.ViewHolder
   {
       CardView cardView;
       ImageView fotoPerfil;
       TextView hora;
       TextView nombre;
       Button aceptar;
       Button rechazar;

       public solicitudHolder(View itemView)
       {
           super(itemView);
           cardView = (CardView) itemView.findViewById(R.id.cardSolicitud);
           fotoPerfil = (ImageView) itemView.findViewById(R.id.fotoDePerfilSolicitud);
           nombre = (TextView) itemView.findViewById(R.id.NombreSolicitud);
           hora = (TextView) itemView.findViewById(R.id.HoraSolicitud);
           aceptar = (Button) itemView.findViewById(R.id.btAceptar);
           rechazar = (Button) itemView.findViewById(R.id.btRechazar);
       }
   }
}
