package com.example.crisp.rchapp.A_Usuarios.ListaDeAmigos;

/**
 * Created by crisp on 29/12/2017.
 */

public class Atributos
{
    private int fotoDePerfil;
    private String nombre;
    private String mensaje;
    private String hora;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Atributos()
    {
    }

    public int getFotoDePerfil() {
        return fotoDePerfil;
    }

    public void setFotoDePerfil(int fotoDePerfil) {
        this.fotoDePerfil = fotoDePerfil;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
